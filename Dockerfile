FROM openjdk:17
COPY build/libs/task2-0.0.1-SNAPSHOT.jar /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/task2-0.0.1-SNAPSHOT.jar"]
