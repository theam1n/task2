package com.example.task2.controller;

import com.example.task2.dto.StudentDto;
import com.example.task2.dto.StudentRequest;
import com.example.task2.service.impl.StudentServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentServiceImpl studentService;

    @PostMapping
    public ResponseEntity<StudentDto> saveStudent(
            @RequestBody StudentRequest studentRequest) {

        StudentDto response = studentService.saveStudent(studentRequest);

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<StudentDto> getStudent(
            @PathVariable Long id) {

        StudentDto response = studentService.getStudent(id);

        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<List<StudentDto>> getAllStudents(){

        List<StudentDto> response = studentService.getAllStudents();

        return ResponseEntity.ok(response);
    }

    @PutMapping
    public ResponseEntity<StudentDto> editStudent(
            @RequestBody StudentDto student) {

        StudentDto response = studentService.editStudent(student);

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable Long id){

        studentService.deleteStudent(id);

    }

}
