package com.example.task2.mapper;

import com.example.task2.dto.StudentDto;
import com.example.task2.dto.StudentRequest;
import com.example.task2.entity.Student;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface StudentMapper {

    public static final StudentMapper INSTANCE = Mappers.getMapper(StudentMapper.class);

    @Mapping(target = "id", ignore = true)
    Student requestToEntity(StudentRequest studentRequest);

    StudentDto entityToResponse(Student student);


}
