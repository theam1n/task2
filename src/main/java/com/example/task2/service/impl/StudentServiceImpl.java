package com.example.task2.service.impl;

import com.example.task2.dto.StudentDto;
import com.example.task2.dto.StudentRequest;
import com.example.task2.entity.Student;
import com.example.task2.exception.NotFoundException;
import com.example.task2.mapper.StudentMapper;
import com.example.task2.repository.StudentRepository;
import com.example.task2.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    private static final Logger logger = LoggerFactory.getLogger(StudentServiceImpl.class);

    @Override
    public StudentDto saveStudent(StudentRequest studentRequest) {
        logger.info("ActionLog.saveStudent.start request: {}",studentRequest);

        var student = StudentMapper.INSTANCE.requestToEntity(studentRequest);
        var response = StudentMapper.INSTANCE.entityToResponse(studentRepository.save(student));

        logger.info("ActionLog.saveStudent.end response: {}",response);

        return response;
    }

    @Override
    public StudentDto getStudent(Long id) {
        logger.info("ActionLog.getStudent.start request: {}",id);

        Optional<Student> optionalStudent = Optional.ofNullable(studentRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Student not found with id: " + id)));

        var student = optionalStudent.get();
        var response = StudentMapper.INSTANCE.entityToResponse(student);

        logger.info("ActionLog.getStudent.end response: {}",response);

        return response;
    }

    @Override
    public List<StudentDto> getAllStudents() {
        logger.info("ActionLog.getAllStudents.start");

        var students = studentRepository.findAll();
        var response = students.stream()
                .map(StudentMapper.INSTANCE:: entityToResponse)
                .collect(Collectors.toList());

        logger.info("ActionLog.getAllStudents.end response: {}",response);

        return response;
    }

    @Override
    public StudentDto editStudent(StudentDto student) {
        logger.info("ActionLog.editStudent.start request: {}",student);

        Optional<Student> optionalStudent = Optional.ofNullable(studentRepository.findById(student.getId())
                .orElseThrow(() -> new NotFoundException("Student not found with id: " + student.getId())));

        var existingStudent = optionalStudent.get();

        Optional.ofNullable(student.getName()).ifPresent(existingStudent::setName);
        Optional.ofNullable(student.getAge()).ifPresent(existingStudent::setAge);

        var response = StudentMapper.INSTANCE
                .entityToResponse(studentRepository.save(existingStudent));

        logger.info("ActionLog.editStudent.end response: {}",response);

        return response;
    }

    @Override
    public void deleteStudent(Long id) {
        logger.info("ActionLog.deleteStudent.start request: {}",id);

        var student = studentRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Student not found with id : " + id));

        studentRepository.deleteById(id);

        logger.info("ActionLog.deleteStudent.end");
    }
}
