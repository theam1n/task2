package com.example.task2.service;

import com.example.task2.dto.StudentDto;
import com.example.task2.dto.StudentRequest;

import java.util.List;

public interface StudentService {

    StudentDto saveStudent(StudentRequest studentRequest);

    StudentDto getStudent(Long id);

    List<StudentDto> getAllStudents();

    StudentDto editStudent(StudentDto student);

    void deleteStudent(Long id);
}
